----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 04.12.2018 16:18:52
-- Design Name: 
-- Module Name: contador - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity count_uds is
 Port (
 clk: in std_logic;
 counting_way: in std_logic;
 
 enable: in std_logic;
 reset: in std_logic;
 count: out std_logic_vector(3 downto 0)
  );
end count_uds;

architecture Behavioral of count_uds is

TYPE ESTATE_T is (S0,S1,S2,S3,S4,S5,S6,S7,S8,S9,SA,SB,SC,SD,SE,SF);
signal estate_register: ESTATE_T;
signal next_estate: ESTATE_T;
begin
 sr:process(clk,reset,enable)
  begin
   if reset='1' then 
   estate_register<=S0;
   if enable='1' then
    elsif rising_edge(clk) then
    estate_register<=next_estate;
    end if;
    end if;
   end process;
   output_dec:process(estate_register)
    begin
    next_estate<=estate_register;
    if counting_way='1' then  --sentido de la cuenta ascendente
     case estate_register is
      when S0=>               
        count<="0000";
        next_estate<=S1;
      when S1=>
        count<="0001";
        next_estate<=S2;
      when S2=>
         count<="0010";
         next_estate<=S3; 
       when S3=>
          count<="0011";
          next_estate<=S4;
        when S4=>
           count<="0100";
           next_estate<=S5; 
        when S5=>
           count<="0101";
           next_estate<=S6;     
        when S6=>
           count<="0110";
           next_estate<=S7;
        when S7=>
           count<="0111";
            next_estate<=S8;
        when S8=>
            count<="1000";
            next_estate<=S9;
        when S9=>
             count<="1001";
             next_estate<=S0;
          
        when OTHERS=>
              next_estate<=S0;
      end case;
      end if;
          if counting_way='0' then --sentido de la cuenta descendente
          case estate_register is
           when S0=>
             count<="0000";
             next_estate<=S0;
           when S1=>
             count<="0001";
             next_estate<=S0;
           when S2=>
              count<="0010";
              next_estate<=S1; 
            when S3=>
               count<="0011";
               next_estate<=S2;
             when S4=>
                count<="0100";
                next_estate<=S3; 
             when S5=>
                count<="0101";
                next_estate<=S4;     
             when S6=>
                count<="0110";
                next_estate<=S5;
             when S7=>
                count<="0111";
                 next_estate<=S6;
             when S8=>
                 count<="1000";
                 next_estate<=S7;
             when S9=>
                  count<="1001";
                  next_estate<=S8;
                       
             when OTHERS=>
                   next_estate<=S0;
           end case;
           end if;
     end process ;


end Behavioral;